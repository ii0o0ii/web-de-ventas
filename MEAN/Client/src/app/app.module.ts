import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from './auth-guard.service';
import { AuthenticationService } from './authentication.service';
import { CajeroComponent } from './cajero/cajero.component';
import { BuscarComponent } from './buscar/buscar.component';
import { VentasComponent } from './ventas/ventas.component';
import { InventarioComponent } from './inventario/inventario.component';

const router: Routes = [
    {path: '', component: LoginComponent},
    {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService],
    children: [
        {path: 'cajero', component: CajeroComponent},
        {path: 'buscar', component: BuscarComponent},
        {path: 'ventas', component: VentasComponent},
        {path: 'inventario', component: InventarioComponent}
    ]}
]

@NgModule({
   declarations: [
      AppComponent,
      LoginComponent,
      DashboardComponent,
      CajeroComponent,
      BuscarComponent,
      VentasComponent,
      InventarioComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule,
      HttpClientModule,
      RouterModule.forRoot(router)
   ],
   providers: [
      AuthenticationService,
      AuthGuardService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
