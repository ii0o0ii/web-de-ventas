import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable, of} from 'rxjs'
import {map} from 'rxjs/operators'
import {Router} from '@angular/router'

export interface UserDetails{
    _id: string,
    name: string,
    username: string,
    password: string,
    exp: number,
    iat: number
}

interface TokenResponse {
    token: string
}

export interface TokenPayload {
    _id: string,
    name: string,
    username: string,
    password: string
}

export interface SearchDetails {
    codigo: string;
    name: string;
    precio_original: string;
    precio_minimo: string;
    precio_cliente: string;
}

@Injectable()
export class AuthenticationService {
    private token: string
    private code: string
    constructor(private http: HttpClient, private router: Router) {}

    private saveToken(token: string): void {
        localStorage.setItem('usertoken', token)    
        this.token = token
    }

    private getToken(): string {
        if(!this.token){
            this.token = localStorage.getItem('usertoken')
        }
        return this.token
    }

    public getUserDetails(): UserDetails {
        const token = this.getToken()
        let payload
        if (token) {
            payload = token.split('.')[1]
            payload = window.atob(payload)
            return JSON.parse(payload)
        } else {
            return null
        }
    }

    public isloggedIn(): boolean {
        const user = this.getUserDetails()
        if(user) {
            return user.exp > Date.now() / 1000
        } else {
            return false
        }
    }

    public register(user: TokenPayload): Observable<any> {
        const base = this.http.post('/api/register', user)
        const requets = base.pipe(
            map((data: TokenResponse) => {
                if (data.token) {
                    this.saveToken(data.token)
                }
                return data
            })
        )
        return requets
    }

    public login(user: TokenPayload): Observable<any> {
        const base = this.http.post('/api/login', user)
        const requets = base.pipe(
            map((data: TokenResponse) => {
                if (data.token) {
                    this.saveToken(data.token)
                }
                return data
            })
        )
        return requets
    }

    public dashboard(): Observable<any> {
        return this.http.get('/api/dashboard', {
            headers: {Authorization: `${this.getToken}`}
        })
    }

    public getSearch() {
        this.http.post('/api/cajero', data)
        .subscribe(res => {
            console.log(res);
            
        },
        err => {
            console.log(err);
            
        })
    }

    public logout(): void {
        this.token = ''
        window.localStorage.removeItem('usertoken')
        this.router.navigateByUrl('/')
    }
}