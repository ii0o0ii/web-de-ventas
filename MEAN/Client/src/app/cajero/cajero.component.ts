import { Component, OnInit } from '@angular/core';
import { AuthenticationService, SearchDetails } from '../authentication.service';
@Component({
  selector: 'app-cajero',
  templateUrl: './cajero.component.html',
  styleUrls: ['./cajero.component.css']
})
export class CajeroComponent implements OnInit {
  
  constructor(private searchSrv: AuthenticationService) { } 
    
  ngOnInit() {
  } 

  credentials = {
    codigo: '',
    name: '',
    precio_original: '',
    precio_minimo: '',
    precio_cliente: ''
  }
  informa = []
  search() {
    console.log(this.searchSrv.getSearch())
  }

  lista = []
  carrito() {
    this.lista.push(JSON.parse(JSON.stringify(this.credentials)))
  }

  eliminar(i) {
    this.lista.splice(i, 1)
  }
}
