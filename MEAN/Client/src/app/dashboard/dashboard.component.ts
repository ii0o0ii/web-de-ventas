import { Component } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../authentication.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent  {

  constructor(private auth: AuthenticationService, private router: Router) {}
  logaut() {
    localStorage.clear()
    this.router.navigateByUrl('/')
  }
  
}
