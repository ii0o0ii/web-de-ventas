import { Component } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../authentication.service'
import { Router } from '@angular/router'

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  credentials: TokenPayload = {
    _id: '',
    name: '',
    username: '',
    password: ''
  }

  constructor(private auth: AuthenticationService, private router: Router) {}

  login() {
    var regex = new RegExp(/^\w+$/g)
    if (this.credentials.username == '' || this.credentials.password == '') {
      var error = document.getElementById('error')
            error.innerHTML = "POR FAVOR RELLENE LOS CAMPOS"
            setTimeout(() => {
              error.innerHTML = ""
            }, 2000);
    } else {
      if (regex.test(this.credentials.username)) {
        this.auth.login(this.credentials).subscribe(
          () => {
            this.router.navigateByUrl('/dashboard')
          },
          () => {
            var error = document.getElementById('error')
            error.innerHTML = "ESTE USUARIO NO EXISTE"
            setTimeout(() => {
              error.innerHTML = ""
            }, 2000);
          }
        )
      } else {
          var error = document.getElementById('error')
          error.innerHTML = "SOLO SE PERMITE NUMEROS, LETRAS, _ y . "
          setTimeout(() => {
            error.innerHTML = ""
          }, 2000);
      }
    }
  }  
}
