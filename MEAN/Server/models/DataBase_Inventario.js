const mongoose = require('mongoose')
const Schema = mongoose.Schema

const InventarioSchema = new Schema({
    codigo: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    pracio_original: {
        type: String,
        require: true
    },
    pracio_minimo: {
        type: String,
        require: true
    },
    pracio_cliente: {
        type: String,
        require: true
    },
    cantidad: {
        type: String,
        require: true
    },
    descuento: {
        type: String,
        require: true
    },
    iva: {
        type: String,
        require: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = Inventario = mongoose.model('inventarios', InventarioSchema)