const express = require('express')
const cajeros = express.Router()
const cors = require('cors')

const Cajero = require('../models/DataBase_Inventario')
cajeros.use(cors())

cajeros.post('/cajero', (req, res) => {
    var regex = new RegExp(/^\w+$/g)
    if (regex.test(req.body.codigo)) {
        Cajero.findOne({
            codigo: req.body.codigo
        })
        .then(code => {
            if (code) {
                const payload = {
                    codigo: code.codigo,
                    name: code.name,
                    precio_original: code.precio_original,
                    precio_minimo: code.precio_minimo,
                    precio_cliente: code.precio_cliente
                }
                res.send(payload)
            } else {
                res.send('Code not found')
            }
        })
    } else {
        console.warn('Special characters are not valid')
        res.json({error: 'Special characters are not valid'})  
    }
})

module.exports = cajeros