const express = require('express')
const logins = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt  = require('bcrypt')

const Login = require('../models/DataBase_User')
logins.use(cors())

process.env.SECRET_KET = 'secret'

logins.post('/login', (req, res) => {
    const today = new Date()
    var regex = new RegExp(/^\w+$/g)
    if (regex.test(req.body.username)) {
        if (Object.keys(req.body.username).length < 5 || Object.keys(req.body.username).length > 10) {
            console.warn('The user must have a minimum of 5 characters and maximum of 10 characters.')
            res.json({error: 'The user must have a minimum of 5 characters and maximum of 10 characters.'})
            var verification = false
        } else  if (Object.keys(req.body.password).length < 5 || Object.keys(req.body.password).length > 10) {
                console.warn('The password must have a minimum of 5 characters and maximum of 10 characters.')
                res.json({error: 'The password must have a minimum of 5 characters and maximum of 10 characters.'})
                var verification = false
        } else {
            var verification = true
        }
        if (verification) {
            Login.findOne({
                username: req.body.username
            })
            .then(user => {
                if(user){
                    if(bcrypt.compareSync(req.body.password, user.password)) {
                        const payload = {
                            _id: user._id,
                            name: user.name,
                            username: user.username,
                            password: user.password   
                        }
                        let token = jwt.sign(payload, process.env.SECRET_KET, {
                            expiresIn: 1440
                        })
                        res.json({token: token})
                        console.info('The user ' + user.username + ' You have logged in ' + today)
                    } else { 
                        res.send('User or Password is incorrect')
                    }
                    
                } else {
                    res.send('User does not exist'+'\n'+ 'error: ' + err)
                }
            })
            .catch(err => {
                res.send('error: ' + err)
            })
        
        } else {
            console.warn('Validation of the fields there was a failure')
            res.json({error: 'Validation of the fields there was a failure'})
        }
    } else {
        console.warn('Special characters are not valid')
        res.json({error: 'Special characters are not valid'})  
    }
})
        
logins.get('/dashboard', (req, res) => {
    var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KET)
    Login.findOne({
        _id: decoded._id
    })
    .then(user => {
        if(user){
            res.json(user)
        } else {
            res.send('User does not exist')
        }
    })
    .catch(err => {
        res.json('error: ' + err)
    })
})
        

module.exports = logins