const express = require('express')
const registers = express.Router()
const cors = require('cors')
const bcrypt  = require('bcrypt')

const Register = require('../models/DataBase_User')
registers.use(cors())

process.env.SECRET_KET = 'secret'

registers.post('/register', (req, res) => {
    const today = new Date()
    const registerData = {
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
        admin: req.body.admin,
        create: today
    }

    Register.findOne({
        username: req.body.username
    })
    .then(user => {
        if(!user) {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                registerData.password = hash
                Register.create(registerData)
                .then(user => {
                    res.json({status: user.name + ' Register sussefull ' + today})
                })
                .catch(err => {
                    res.send('error: ' + err)
                })
            })
        } else {
            res.json({error: 'User already exists'})
        }
    })
    .catch(err => {
        res.send('error: ' + err)
    })
})


module.exports = registers 