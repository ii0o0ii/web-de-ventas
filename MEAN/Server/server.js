var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var app = express()
var mongoose = require('mongoose')
var port = process.env.PORT || 3000


app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({extended: false}))


const mongoURI = 'mongodb://localhost:27017/ps_userdata'

mongoose
    .connect(mongoURI, {useNewUrlParser: true})
    .then(() => console.log("MongoDB connection Succefull"))
    .catch(err => console.log(err))

var Register = require("./routes/Registers")
var Login = require("./routes/Logins")
var Cajero = require("./routes/Cajeros")

app.use('/api', Register)
app.use('/api', Login)
app.use('/api', Cajero)

app.listen(port, function () {
    console.log("Server is running on port: " + port)
})